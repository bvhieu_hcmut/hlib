/**
 \file huart.h
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Define functions to handle UART
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#ifndef __HUART_H
#define __HUART_H

#include "stm32f1xx_hal.h"
#include "htype.h"
#include "hconf.h"


#if defined PLATFORM_BANYAN_BOARD
    #define NUM_UART                (3U)
    #define UART2_TX_PIN_INDEX      (7U)
    #define UART2_RX_PIN_INDEX      (8U)
    #define UART3_TX_PIN_INDEX      (22U)
    #define UART3_RX_PIN_INDEX      (23U)
#endif


#define UART_TRANSMIT_TIMEOUT		(0xFFFF)
#define UART_RECV_BUF_SIZE          (256U) 
/////////////////////////////////////////////////////////////////////////////////////////////////////
class huart_c{
public:
    huart_c(uint8_t uartIndex);
    void Start(uint32_t baudRate);
    void Print(char c);
    void Print(char* str);
    void Print(int32_t num);
    void Print(uint32_t num);
    void Print(char* preStr, int32_t num);
    void Print(char* preStr, uint32_t num);
    void Print(char* preStr, char* str);
    void Println(char c);
    void Println(char* str);
    void Println(int32_t num);
    void Println(uint32_t num);
    void Println(char* preStr, int32_t num);
    void Println(char* preStr, uint32_t num);
    void Println(char* preStr, char* str);
    void Write(uint8_t byte);
    void Write(uint8_t buf[], uint16_t length);

    uint16_t Available(void);
    uint8_t  Read(void);
    uint16_t Read(uint8_t buf[], uint16_t maxLength);
private:
    uint8_t              _uartIndex;
    uint16_t             _tail, _head;
    uint8_t              _recvBuf[UART_RECV_BUF_SIZE];
    void                 RegisterBuf(uint8_t uartIndex, uint8_t *pRecvBuf, uint16_t* pHead);
};

#endif /* __HUART_H */
