/**
 \file hpin.h
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Define functions to control pin on HLib's base board
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#ifndef __HPIN_H
#define __HPIN_H

#include "hconf.h"
#include "htype.h"


/////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined PLATFORM_BANYAN_BOARD
	#define NUM_PIN		(36)
#endif

/////////////////////////////////////////////////////////////////////////////////////////////////////
typedef enum{
    ANALOG,
    OUTPUT,
    OUT_OPEN_DRAIN,
    INPUT,
    IN_PULL_UP,
    IN_PULL_DOWN,
    PERIPHERAL,
    PERIPHERAL_OPEN_DRAIN
} hpin_mode_t;


/////////////////////////////////////////////////////////////////////////////////////////////////////
bool_t  HPIN_SetMode(uint8_t pinIndex, hpin_mode_t pinMode);
bool_t  HPIN_Write(uint8_t pinIndex, bool_t pinVal);
bool_t  HPIN_WriteOne(uint8_t pinIndex);
bool_t  HPIN_WriteZero(uint8_t pinIndex);
bool_t  HPIN_Toggle(uint8_t pinIndex);
bool_t  HPIN_Read(uint8_t pinIndex);


#endif /* __HPIN_H */
