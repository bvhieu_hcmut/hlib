/**
 \file huart.cpp
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Implement functions to handle UART
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#include "huart.h"
#include "hstru.h"
#include "hpin.h"
#include <string.h>

/////////////////////////////////////////////////////////////////////////////////////////////////////
static char       _crLf[] = "\r\n";
static uint8_t    *_pRecvBuf1 = NULL;
static uint8_t    *_pRecvBuf2 = NULL;
static uint8_t    *_pRecvBuf3 = NULL;
static uint16_t   *_pHead1 = NULL;
static uint16_t   *_pHead2 = NULL;
static uint16_t   *_pHead3 = NULL;
static uint8_t    data1, data2, data3;
static UART_HandleTypeDef   _uartHandle[NUM_UART];
/////////////////////////////////////////////////////////////////////////////////////////////////////
//============================================================================
/**
    \brief Construct UART
    \param[in] uartIndex The index of UART (valid values for Banyang board are 1, 2, 3)
    \return None
*/
huart_c::huart_c(uint8_t uartIndex){
    _uartIndex = uartIndex;
}


//============================================================================
/**
    \brief Start the UART with setting baud rate
    \param[in] baudRate The baud rate of UART
    \return None
*/
void huart_c::Start(uint32_t baudRate){
    GPIO_InitTypeDef  GPIO_InitStruct;

    switch (_uartIndex){
        case 1: 
            /* config USART1 pins */
            GPIO_InitStruct.Pin       = GPIO_PIN_9;
            GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
            GPIO_InitStruct.Pull      = GPIO_PULLUP;
            GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
            HAL_GPIO_Init(GPIOA, &GPIO_InitStruct); //Tx pin
            GPIO_InitStruct.Pin = GPIO_PIN_10;
            HAL_GPIO_Init(GPIOA, &GPIO_InitStruct); //Rx pin; break;
            /* enable USART clock */
            __HAL_RCC_USART1_CLK_ENABLE();
            HAL_NVIC_SetPriority(USART1_IRQn, 0, 1);
            HAL_NVIC_EnableIRQ(USART1_IRQn);
            _uartHandle[0].Instance = USART1;
        break;
        
        case 2:
            /* config USART2 pins */ 
            HPIN_SetMode(UART2_TX_PIN_INDEX, PERIPHERAL);
            HPIN_SetMode(UART2_RX_PIN_INDEX, INPUT);
            /* enable USART clock */
            __HAL_RCC_USART2_CLK_ENABLE();
            HAL_NVIC_SetPriority(USART2_IRQn, 0, 1);
            HAL_NVIC_EnableIRQ(USART2_IRQn);
            _uartHandle[_uartIndex-1].Instance = USART2;          
            HAL_UART_Receive_IT(&_uartHandle[_uartIndex-1], &data2, 1); //register to receive next data
        break;
        
        case 3:
            /* config USART3 pins */ 
            HPIN_SetMode(UART3_TX_PIN_INDEX, PERIPHERAL);
            HPIN_SetMode(UART3_RX_PIN_INDEX, INPUT);
            /* enable USART clock */
            __HAL_RCC_USART3_CLK_ENABLE();
            HAL_NVIC_SetPriority(USART3_IRQn, 0, 1);
            HAL_NVIC_EnableIRQ(USART3_IRQn);
            _uartHandle[_uartIndex-1].Instance = USART3; 
            HAL_UART_Receive_IT(&_uartHandle[_uartIndex-1], &data3, 1); //register to receive next data
        break;

        default: return;
    } //end of switch()

    RegisterBuf(_uartIndex, _recvBuf, &_head);
    _uartHandle[_uartIndex-1].Init.WordLength = UART_WORDLENGTH_8B;
    _uartHandle[_uartIndex-1].Init.StopBits   = UART_STOPBITS_1;
    _uartHandle[_uartIndex-1].Init.Parity     = UART_PARITY_NONE;
    _uartHandle[_uartIndex-1].Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    _uartHandle[_uartIndex-1].Init.Mode       = UART_MODE_TX_RX;
    _uartHandle[_uartIndex-1].Init.BaudRate   = baudRate;
    _tail = 0;
    _head = 0;
    HAL_UART_DeInit(&_uartHandle[_uartIndex-1]);
    HAL_UART_Init(&_uartHandle[_uartIndex-1]);
    HAL_UART_Receive_IT(&_uartHandle[0], &data1, 1); //register to receive next data  
    __HAL_USART_ENABLE_IT(&_uartHandle[_uartIndex-1], USART_IT_RXNE);
}


//============================================================================
/**
    \brief Print a character to UART
    \param[in] c The character to print
    \return None
*/
void huart_c::Print(char c){
    HAL_UART_Transmit(&_uartHandle[_uartIndex-1], (uint8_t*) &c, 1, UART_TRANSMIT_TIMEOUT);
}


//============================================================================
/**
    \brief Print a string to UART
    \param[in] str The string to print
    \return None
*/
void huart_c::Print(char* str){
    HAL_UART_Transmit(&_uartHandle[_uartIndex-1], (uint8_t*) str, strlen(str), UART_TRANSMIT_TIMEOUT);
}


//============================================================================
/**
    \brief Print a signed number to UART
    \param[in] num The number to print
    \return None
*/
void huart_c::Print(int32_t num){
    if (num < 0){
        Print('-');
        Print((uint32_t) -num);
    }
    else{
        Print((uint32_t) num);
    }
}


//============================================================================
/**
    \brief Print an unsigned number to UART
    \param[in] num The number to print
    \return None
*/
void huart_c::Print(uint32_t num){
    char outStr[33];
    HSTRU_FromNum(num, 10, outStr);
    Print(outStr);
}


//============================================================================
/**
    \brief Print an unsigned number with a pre-string to UART
    \param[in] preString The pre-string
    \param[in] num The number to print
    \return None
*/
void huart_c::Print(char* preStr, int32_t num){
    Print(preStr);
    Print(num);
}


//============================================================================
/**
    \brief Print an unsigned number with a pre-string to UART
    \param[in] preString The pre-string
    \param[in] num The number to print
    \return None
*/
void huart_c::Print(char* preStr, uint32_t num){
       Print(preStr);
    Print(num);
}
 


//============================================================================
/**
    \brief Print an string with pre-string to UART
    \param[in] preStr The pre-string
    \param[in] str The string to print
    \return None
*/
void huart_c::Print(char* preStr, char* str){
    Print(preStr);
    Print(str);
}



//============================================================================
/**
    \brief Print a character with new line to UART
    \param[in] c The character to print
    \return None
*/
void huart_c::Println(char c){
    Print(c);
    Print(_crLf);
}


//============================================================================
/**
    \brief Print a string with new line to UART
    \param[in] str The string to print
    \return None
*/
void huart_c::Println(char* str){
    Print(str);
    Print(_crLf);
}


//============================================================================
/**
    \brief Print a signed number with new line to UART
    \param[in] num The number to print
    \return None
*/
void huart_c::Println(int32_t num){
    Print(num);
    Print(_crLf);
}


//============================================================================
/**
    \brief Print an unsigned number with new line to UART
    \param[in] The number to print
    \return None
*/
void huart_c::Println(uint32_t num){
    Print(num);
    Print(_crLf);
}


//============================================================================
/**
    \brief Print a signed number with pre-string and new line to UART
    \param[in] preStr The pre-string
    \param[in] num The number to print
    \return None
*/
void huart_c::Println(char* preStr, int32_t num){
    Print(preStr);
    Println(num);
}


//============================================================================
/**
    \brief Print an unsigned number with pre-string and new line to UART
    \param[in] preStr The pre-string
    \param[in] num The number to print
    \return None
*/
void huart_c::Println(char* preStr, uint32_t num){
    Print(preStr);
    Println(num);
}


//============================================================================
/**
    \brief Print an string with pre-string and new line to UART
    \param[in] preStr The pre-string
    \param[in] The number to print
    \return None
*/
void huart_c::Println(char* preStr, char* str){
    Print(preStr);
    Println(str);
}


//============================================================================
/**
    \brief Send a byte to UART
    \param[in] byte The byte to send
    \return None
*/
void huart_c::Write(uint8_t byte){
    HAL_UART_Transmit(&_uartHandle[_uartIndex-1], &byte, 1, UART_TRANSMIT_TIMEOUT);
}


//============================================================================
/**
    \brief Send a buffer to UART
    \param[in] buf The buffer to send
    \param[in] length The length of buffer
    \return None
*/
void huart_c::Write(uint8_t buf[], uint16_t length){
    HAL_UART_Transmit(&_uartHandle[_uartIndex-1], buf, length, UART_TRANSMIT_TIMEOUT);
}


//=============================================================================
/**
    \brief Check the number of bytes which are received in receiving buffer
    \return The number of bytes in receiving buffer
*/
uint16_t huart_c::Available(void){
    return (uint16_t) (((((uint32_t) _head) + UART_RECV_BUF_SIZE) - _tail) % UART_RECV_BUF_SIZE);
}


//============================================================================
/**
    \brief Read a byte from receiving buffer
    \return The oldest received data in receiving buffer
    \attention Need to check Available() before this call or return data may be incorrect
*/
uint8_t huart_c::Read(void){
    uint8_t retData = 0;
    if (_tail != _head){
        retData = _recvBuf[_tail];
        _tail = (_tail + 1) % UART_RECV_BUF_SIZE;
    }

    return retData;
}


//============================================================================
/**
    \brief Read a array of bytes from receiving buffer
    \param[out] buf The buffer to receive read data
    \param[in] maxLength The maximum number of byte to read
    \return The actual number of byte was read
    \attention Buffer have to be large enough to store read data
*/
uint16_t huart_c::Read(uint8_t buf[], uint16_t maxLength){
    uint16_t numRead;

    numRead = 0;
    while ((Available()) && (numRead < maxLength)){
        buf[numRead] = Read();
        numRead++;
    }

    return numRead;
}



void huart_c::RegisterBuf(uint8_t uartIndex, uint8_t *pRecvBuf, uint16_t* pHead){
    switch (uartIndex){
        case 1: _pRecvBuf1 = pRecvBuf; _pHead1 = pHead; break;
        case 2: _pRecvBuf2 = pRecvBuf; _pHead2 = pHead; break;
        case 3: _pRecvBuf3 = pRecvBuf; _pHead3 = pHead; break;
        default: /* do nothing */ break;
    }
}


/////////////////////////////////////////////////////////////////////////
////    Interrupt IRQ    ////////////
extern "C"{
    void USART1_IRQHandler(void){
        HAL_UART_IRQHandler(&(_uartHandle[0]));
    }

    void USART2_IRQHandler(void){
        HAL_UART_IRQHandler(&(_uartHandle[1]));
    }

    void USART3_IRQHandler(void){
        HAL_UART_IRQHandler(&(_uartHandle[2]));
    }

    void HAL_UART_RxCpltCallback(UART_HandleTypeDef *uartHandle){
        if (uartHandle->Instance == USART1){
            _pRecvBuf1[*_pHead1] = data1;    //copy data to buffer
            *_pHead1 = (*_pHead1+1) % UART_RECV_BUF_SIZE;
            HAL_UART_Receive_IT(uartHandle, &data1, 1); //register to receive next data
        }
        else if (uartHandle->Instance == USART2){
            _pRecvBuf2[*_pHead2] = data2;    //copy data to buffer
            *_pHead2 = (*_pHead2+1) % UART_RECV_BUF_SIZE;
            HAL_UART_Receive_IT(uartHandle, &data2, 1); //register to receive next data
        }
        else if (uartHandle->Instance == USART3){
            _pRecvBuf3[*_pHead3] = data3;    //copy data to buffer
            *_pHead3 = (*_pHead3+1) % UART_RECV_BUF_SIZE;
            HAL_UART_Receive_IT(uartHandle, &data3, 1); //register to receive next data
        }
        
    }
}   /* end extern "C" */
