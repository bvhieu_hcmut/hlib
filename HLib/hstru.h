/**
 \file hstru.h
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Define string utility functions for HLib
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#ifndef __HSTRU_H
#define __HSTRU_H

#include "htype.h"


/////////////////////////////////////////////////////////////////////////////////////////////////////
void  HSTRU_FromNum(uint32_t printNum, uint8_t radix, char* pOutStr);

#endif /* __HSTRU_H */
