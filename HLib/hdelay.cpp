/**
 \file 
 \author
 \version 1.0
 \date 
 \brief 
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/

#include "hdelay.h"

//====================================================================
/**
	\brief
	\para
	\return
*/
void HDELAY_Reinit(void){

}


//====================================================================
/**
	\brief Delay micro second
	\para
	\return
*/
void HDelay(uint32_t us){

}