/**
 \file hlcd.h
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Implement function to control character LCD in 4-bit mode
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#include "hpin.h"
#include "hlcd.h"
#include "hlib.h"


volatile static uint8_t _optFreeVar;
/**
  * @brief Construction function. Do nothing
  * @return None
  */
hlcd_c::hlcd_c(void){
}


/** @brief Initialize all HD44780 control pins, set default mode, and return home
  * @retval None
  */
void hlcd_c::Start(uint8_t d4Pin, uint8_t d5Pin, uint8_t d6Pin, uint8_t d7Pin, uint8_t enPin, uint8_t rsPin, uint8_t blPin){
    _d4Pin = d4Pin; HPIN_SetMode(_d4Pin, OUTPUT); 
    _d5Pin = d5Pin; HPIN_SetMode(_d5Pin, OUTPUT); 
    _d6Pin = d6Pin; HPIN_SetMode(_d6Pin, OUTPUT); 
    _d7Pin = d7Pin; HPIN_SetMode(_d7Pin, OUTPUT); 
    _rsPin = rsPin; HPIN_SetMode(_rsPin, OUTPUT); 
    _enPin = enPin; HPIN_SetMode(_enPin, OUTPUT); HPIN_WriteZero(_enPin);
    _blPin = blPin; HPIN_SetMode(_blPin, OUTPUT); HPIN_WriteOne(_blPin);
    
    
    HLoopDelay(1000000);    //wait LCD stable
    FunctionSet(false, false);
    FunctionSet(false, false);
    OnOff(true, true, true);
    EntryMode(false, true);
    Clear();
    Home();
    HLoopDelay(1000000);
    Print("Wellcome to HLib");
}


/**
  * @brief Send Clear Display command
  * @return None
  */
void hlcd_c::Clear(void){
    Write(HLCD_CLEAR_DISPLAY_CMD, true);
    HLoopDelay(HLCD_CLEAR_DISPLAY_DELAY);  
}



/**
  * @brief Send Cursor Home command
  * @return None
  */
void hlcd_c::Home(void){
    Write(HLCD_CURSOR_HOME_CMD, true);
    HLoopDelay(HLCD_CURSOR_HOME_DELAY);  
}



/**
  * @brief Send Entry Mode command
  * @param shift TRUE the display will be shifted, FALSE the display will not be shifted
  * @param increase TRUE increase cursor position, FALSE decrease cursor position
  * @return None
  */
void hlcd_c::EntryMode(bool shift, bool increase){
    uint8_t writtenData = HLCD_ENTRY_MODE_CMD;
    if (shift){
        writtenData |= 0x01;
    }
    if (increase){
        writtenData |= 0x02;
    }
    Write(writtenData, true);
    HLoopDelay(HLCD_ENTRY_MODE_DELAY);
}



/**
  * @brief Send Display On Off command
  * @param displayOn TRUE set the display on, FALSE set the display off
  * @param cursorOn  TRUE set the cursor on, FALSE set the cursor off
  * @param cursorBlink TRUE the cursor is blinked, FALSE the cursor is not blinked
  * @return None
  */
void hlcd_c::OnOff(bool displayOn, bool cursorOn, bool cursorBlink){
    uint8_t writtenData = HLCD_DISPLAY_ON_OFF_CMD;
    if (displayOn){
        writtenData |= 0x04;
    }
    if (cursorOn){
        writtenData |= 0x02;
    }
    if (cursorBlink){
        writtenData |= 0x01;
    }
    Write(writtenData, true);
    HLoopDelay(HLCD_DISPLAY_ON_OFF_DELAY);
}



/**
  * @brief Set display cursor to a specified position
  * @param charLine LCD's line
  * @param charCol LCD's column
  * @return None
  */
void hlcd_c::Goto(uint8_t charLine, uint8_t charCol){
    uint8_t absAddr;
    absAddr = (charLine * 0x40) + charCol;
    Write((absAddr & 0xEF) | HLCD_SET_DDRAM_ADDRESS_CMD, true);
    HLoopDelay(HLCD_SET_DDRAM_ADDRESS_DELAY);
}
 



/**
  * @brief Print one character to the LCD
  * @param printChar Character to print
  * @return None
  */
void hlcd_c::Print(char printChar){
    Write(printChar, false);   
    HLoopDelay(200);
}



/**
  * @brief Print one string to the LCD
  * @param printString String to print
  * @return None
  */
void hlcd_c::Print(char* printString){
    while(*printString != '\0'){
        Print(*printString);
        printString++;
    }
}

    
/**
  * @brief Print one unsigned number to the LCD
  * @param printNum Unsigned number to print
  * @param radix Valid values are 2, 8, 10, 16
  * @return None
  */
void hlcd_c::Print(uint32_t printNum, uint8_t radix){
    char outStr[33];
    HSTRU_FromNum(printNum, radix, outStr);
    Print((char*) outStr);
}


/**
  * @overload
  * @brief Print one unsigned number to the LCD in decimal
  * @param printNum Unsigned number to print
  * @return None
  */
void hlcd_c::Print(uint32_t printNum){
    Print(printNum, (uint8_t) 10);
}



/**
  * @brief Print one signed number to the LCD in decimal
  * @param printNum Unsigned number to print
  * @return None
  */
void hlcd_c::Print(int32_t printNum){
    if (printNum<0){
        Print('-');
        Print((uint32_t) -printNum, (uint8_t) 10);
    }
    else{
        Print((uint32_t) printNum, (uint8_t) 10);
    }
}

/**
  * @brief Write a data/command to LCD
  * @param data Written data
  * @param isCmd TRUE written is command, FALSE written is display data
  * @return none
  */
void hlcd_c::Write(uint8_t data, bool isCmd){
    if (isCmd){
        HPIN_WriteZero(_rsPin);
    }
    else{
        HPIN_WriteOne(_rsPin);
    }

  
    /* write higher nibble */
  	if (data & 0x80) {HPIN_WriteOne(_d7Pin);} else {HPIN_WriteZero(_d7Pin);}
    if (data & 0x40) {HPIN_WriteOne(_d6Pin);} else {HPIN_WriteZero(_d6Pin);}
    if (data & 0x20) {HPIN_WriteOne(_d5Pin);} else {HPIN_WriteZero(_d5Pin);}
    if (data & 0x10) {HPIN_WriteOne(_d4Pin);} else {HPIN_WriteZero(_d4Pin);}
    _optFreeVar++; /* NOP */
    HPIN_WriteOne(_enPin);
    _optFreeVar++;
    HPIN_WriteZero(_enPin);
    _optFreeVar++;
    

    /* write lower nibble */ 
    if (data & 0x08) {HPIN_WriteOne(_d7Pin);} else {HPIN_WriteZero(_d7Pin);}
    if (data & 0x04) {HPIN_WriteOne(_d6Pin);} else {HPIN_WriteZero(_d6Pin);}
    if (data & 0x02) {HPIN_WriteOne(_d5Pin);} else {HPIN_WriteZero(_d5Pin);}
    if (data & 0x01) {HPIN_WriteOne(_d4Pin);} else {HPIN_WriteZero(_d4Pin);}
    _optFreeVar++; /* NOP */
    HPIN_WriteOne(_enPin);
    _optFreeVar++;
    HPIN_WriteZero(_enPin);
}


/**
  * @brief Send FunctionSet command
  * @param eightBit TRUE 8-bit is selected, FALSE 4-bit is selected
  * @param font5x10 TRUE 5x10 font is selected, FALSE 5x7 font is selected
  * @return None
  */
void hlcd_c::FunctionSet(bool eightBit, bool font5x10){
  uint8_t writtenData = HLCD_FUNCTION_SET_CMD;
  if (eightBit){
    writtenData |= 0x10;
  }
  writtenData |= 0x08; /* two lines */
  if (font5x10){
    writtenData |= 0x04;
  }
  Write(writtenData, true);
  HLoopDelay(HLCD_FUNCTION_SET_DELAY);
}
