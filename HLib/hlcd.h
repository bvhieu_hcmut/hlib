/**
 \file hlcd.h
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Define functions to control character LCD in 4-bit mode
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#ifndef __HLCD_H
#define __HLCD_H

#include "htype.h"


/////////////////////////////////////////////////////////////////////////////////////////////////////
#define HLCD_CLEAR_DISPLAY_CMD           (0x01U) /**< Clear display command*/
#define HLCD_CURSOR_HOME_CMD             (0x02U) /**< Set cursor to home command*/
#define HLCD_ENTRY_MODE_CMD              (0x04U) /**< Set entry mode command*/
#define HLCD_DISPLAY_ON_OFF_CMD          (0x08U) /**< Set display ON/OFF command*/
#define HLCD_CURSOR_DISPLAY_SHIFT_CMD    (0x10U) /**< Set cursor shift command*/
#define HLCD_FUNCTION_SET_CMD            (0x20U) /**< Set function mode command*/
#define HLCD_SET_CGRAM_ADDRESS_CMD       (0x40U) /**< Set CGRAM addresss command*/
#define HLCD_SET_DDRAM_ADDRESS_CMD       (0x80U) /**< Set DDRAM address command*/

#define HLCD_CLEAR_DISPLAY_DELAY         (24000U)   /**< Delay time after clear display command*/ 
#define HLCD_CURSOR_HOME_DELAY           (24000U)   /**< Delay time after set cursor to home command*/ 
#define HLCD_ENTRY_MODE_DELAY            (24000U)   /**< Delay time after set entry mode command*/ 
#define HLCD_DISPLAY_ON_OFF_DELAY        (24000U)   /**< Delay time after set display ON/OFF command*/ 
#define HLCD_CURSOR_DISPLAY_SHIFT_DELAY  (24000U)   /**< Delay time after set cursor shift command*/ 
#define HLCD_FUNCTION_SET_DELAY          (400000U)  /**< Delay time after set function mode command*/ 
#define HLCD_SET_CGRAM_ADDRESS_DELAY     (200U)         /**< Delay time after set CGRAM address command*/ 
#define HLCD_SET_DDRAM_ADDRESS_DELAY     (200U)         /**< Delay time after set DDRAM address command*/ 


/////////////////////////////////////////////////////////////////////////////////////////////////////
class hlcd_c {
public:
    hlcd_c(void);
    void Start(uint8_t d4Pin, uint8_t d5Pin, uint8_t d6Pin, uint8_t d7Pin, uint8_t enPin, uint8_t rsPin, uint8_t blPin);
    void Clear(void);
    void Home(void);
    void FunctionSet(bool eightBit, bool font5x10);
    void EntryMode(bool shift, bool increase);
    void OnOff(bool displayOn, bool cursorOn, bool cursorBlink);
    void Goto(uint8_t row, uint8_t col);
    
    void Print(char printChar);
    void Print(char* printStr);
    void Print(int32_t printNum);
    void Print(uint32_t pinrtNum);
    void Print(uint32_t printNum, uint8_t radix);
private:
    uint8_t _d4Pin, _d5Pin, _d6Pin, _d7Pin, _rsPin, _enPin,_blPin;
    void Write(uint8_t data, bool isCmd);
};

#endif /* __HLCD_H */
