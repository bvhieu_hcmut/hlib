/**
 \file hpin.cpp
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Implement functions to control pins of HLib's base boards
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#include "stm32f1xx_hal.h"
#include "hpin.h"


/////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined PLATFORM_BANYAN_BOARD
	static GPIO_TypeDef* sArrGPIO[NUM_PIN] = {
							GPIOC, GPIOC, GPIOC, GPIOC,
							GPIOC, GPIOA, GPIOA, GPIOA, GPIOA,
							GPIOC, GPIOA, GPIOA, GPIOA, GPIOA,
							GPIOC, GPIOB, GPIOB, GPIOB, GPIOB,
							GPIOC, GPIOA, GPIOB, GPIOB, GPIOB,
							GPIOC, GPIOB, GPIOB, GPIOB, GPIOB,
							GPIOC, GPIOB, GPIOB, GPIOB, GPIOB,
							GPIOC, GPIOC }
						;
	static const uint16_t sArrPin[NUM_PIN]  = { 
							GPIO_PIN_0,  GPIO_PIN_1,  GPIO_PIN_2,  GPIO_PIN_3,
							GPIO_PIN_5,  GPIO_PIN_0,  GPIO_PIN_1,  GPIO_PIN_2,  GPIO_PIN_3,
							GPIO_PIN_11, GPIO_PIN_4,  GPIO_PIN_5,  GPIO_PIN_6,  GPIO_PIN_7,
							GPIO_PIN_6,  GPIO_PIN_12, GPIO_PIN_13, GPIO_PIN_14, GPIO_PIN_15,
							GPIO_PIN_10, GPIO_PIN_15, GPIO_PIN_3,  GPIO_PIN_10, GPIO_PIN_11,
							GPIO_PIN_7,  GPIO_PIN_1,  GPIO_PIN_0,  GPIO_PIN_5,  GPIO_PIN_4,
							GPIO_PIN_13, GPIO_PIN_6,  GPIO_PIN_7,  GPIO_PIN_8,  GPIO_PIN_9,
              				GPIO_PIN_4,  GPIO_PIN_9  
              			};										  
#endif



/////////////////////////////////////////////////////////////////////////////////////////////////////
//============================================================================
/**
	\brief Set mode of a pin to OUTPUT, INPUT, etc
	\param[in] pinIndex Index of the pin (look on the board)
	\param[in] pinMode Mode of the pin
	\return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/	
bool_t HPIN_SetMode(uint8_t pinIndex, hpin_mode_t pinMode){
	GPIO_InitTypeDef initPinDef;

	if (pinIndex >= NUM_PIN){ return FALSE_V; }
	switch (pinMode){
		case ANALOG:
			initPinDef.Mode = GPIO_MODE_ANALOG ;
			initPinDef.Pull = GPIO_NOPULL;
			break;

		case OUTPUT:
			initPinDef.Mode = GPIO_MODE_OUTPUT_PP ;
			initPinDef.Pull = GPIO_NOPULL;
			break;
			
		case OUT_OPEN_DRAIN:
			initPinDef.Mode = GPIO_MODE_OUTPUT_OD  ;
			initPinDef.Pull = GPIO_NOPULL;
			break;
			
		case INPUT:
			initPinDef.Mode = GPIO_MODE_INPUT;
			initPinDef.Pull = GPIO_NOPULL;
			break;
			
		case IN_PULL_UP:
			initPinDef.Mode = GPIO_MODE_INPUT;
			initPinDef.Pull = GPIO_PULLUP;
			break;
			
		case IN_PULL_DOWN:
			initPinDef.Mode = GPIO_MODE_INPUT;
			initPinDef.Pull = GPIO_PULLDOWN;
			break;
			
		case PERIPHERAL:
			initPinDef.Mode = GPIO_MODE_AF_PP ;
			initPinDef.Pull = GPIO_NOPULL;
			break;
			
		case PERIPHERAL_OPEN_DRAIN:
			initPinDef.Mode = GPIO_MODE_AF_OD ;
			initPinDef.Pull = GPIO_NOPULL;
			break;

		default: return FALSE_V;
	}

	initPinDef.Pin = sArrPin[pinIndex];
	initPinDef.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(sArrGPIO[pinIndex], &initPinDef);
	return TRUE_V;
}


//============================================================================
/**
	\brief Write a new value to a pin (pin has to be OUTPUT or OUT_OPEN_DRAIN)
	\param[in] pinIndex Index of the pin
	\param[pinVal] New value of the pin
	\return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/
bool_t HPIN_Write(uint8_t pinIndex, bool_t pinVal){
	if (pinIndex >= NUM_PIN){ return FALSE_V; }
	if (pinVal){
		HAL_GPIO_WritePin(sArrGPIO[pinIndex], sArrPin[pinIndex], GPIO_PIN_SET);
	}
	else{
		HAL_GPIO_WritePin(sArrGPIO[pinIndex], sArrPin[pinIndex], GPIO_PIN_RESET);
	}
	return TRUE_V;
}


//============================================================================
/**
	\brief Write value one (1) to a pin (pin has to be OUTPUT or OUT_OPEN_DRAIN)
	\param[in] pinIndex Index of the pin
	\return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/
bool_t HPIN_WriteOne(uint8_t pinIndex){
	return HPIN_Write(pinIndex, TRUE_V);
}


//============================================================================
/**
	\brief Write value zero (0) to a pin (pin has to be OUTPUT or OUT_OPEN_DRAIN)
	\param[in] pinIndex Index of the pin
	\return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/
bool_t  HPIN_WriteZero(uint8_t pinIndex){
	return HPIN_Write(pinIndex, FALSE_V);
}


//============================================================================
/**
	\brief Toggle value of a pin
	\param[in] pinIndex Index of the pin
	\return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/
bool_t HPIN_Toggle(uint8_t pinIndex){
	if (pinIndex >= NUM_PIN){ return FALSE_V; }
	HAL_GPIO_TogglePin(sArrGPIO[pinIndex], sArrPin[pinIndex]);
	return TRUE_V;  
}


//============================================================================
/**
	\brief Read current value of a pin
	\param[in] pinIndex Index of the pin
	\return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/
bool_t HPIN_Read(uint8_t pinIndex){
	if (pinIndex >= NUM_PIN){ return FALSE_V; }
	return (bool_t) HAL_GPIO_ReadPin(sArrGPIO[pinIndex], sArrPin[pinIndex]);
}
