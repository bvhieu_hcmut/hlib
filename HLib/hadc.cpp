/**
 \file hacd.cpp
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Implement functions read ADC on HLib's base board
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/

#include "stm32f1xx_hal.h"
#include "hadc.h"



/////////////////////////////////////////////////////////////////////////////////////////////////////
ADC_HandleTypeDef       _adcHandle;


/////////////////////////////////////////////////////////////////////////////////////////////////////
/**
    \brief Reinit ADC library
    \return NONE
*/
void HADC_Reinit(void){
    __HAL_RCC_ADC1_CLK_ENABLE();
    //config ...
    _adcHandle.Instance = ADC1;
     HAL_ADC_DeInit(&_adcHandle);
    _adcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
    _adcHandle.Init.ScanConvMode          = ADC_SCAN_DISABLE;              /* Sequencer disabled (ADC conversion on only 1 channel: channel set on rank 1) */
    _adcHandle.Init.ContinuousConvMode    = DISABLE;                       /* Continuous mode disabled to have only 1 conversion at each conversion trig */
    _adcHandle.Init.NbrOfConversion       = 1;                             /* Parameter discarded because sequencer is disabled */
    _adcHandle.Init.DiscontinuousConvMode = DISABLE;                       /* Parameter discarded because sequencer is disabled */
    _adcHandle.Init.NbrOfDiscConversion   = 1;                             /* Parameter discarded because sequencer is disabled */
    _adcHandle.Init.ExternalTrigConv      = ADC_SOFTWARE_START;            /* Software start to trig the 1st conversion manually, without external event */
    HAL_ADC_Init(&_adcHandle);

    /* calibrate ADC */
    HAL_ADCEx_Calibration_Start(&_adcHandle);    
}


/**
    \brief Set active pin to read ADC later
    \param[in] pinIndex Index of the pin (look on the board)
    \return FALSE_V if pinIndex>= MAX_SUPPORT_CHANNEL; TRUE_V otherwise 
*/
bool_t HADC_SetReadPin(uint8_t pinIndex){
    ADC_ChannelConfTypeDef  channelConf;

    if (pinIndex >= MAX_SUPPORT_CHANNEL) { return FALSE_V; }
    switch (pinIndex){
        case 0: channelConf.Channel = ADC_CHANNEL_10; break;
        case 1: channelConf.Channel = ADC_CHANNEL_11; break;
        case 2: channelConf.Channel = ADC_CHANNEL_12; break;
        case 3: channelConf.Channel = ADC_CHANNEL_13; break;
        case 4: channelConf.Channel = ADC_CHANNEL_15; break;
        case 5: channelConf.Channel = ADC_CHANNEL_0;  break;
        case 6: channelConf.Channel = ADC_CHANNEL_1;  break;
        case 7: channelConf.Channel = ADC_CHANNEL_2;  break;
        case 8: channelConf.Channel = ADC_CHANNEL_3;  break;
        default : break;
    }
    
    channelConf.Rank         = ADC_REGULAR_RANK_1;
    channelConf.SamplingTime = ADC_SAMPLETIME_41CYCLES_5;
    HAL_ADC_ConfigChannel(&_adcHandle, &channelConf);
 
    return TRUE_V;
}


//============================================================================
/**
	\brief Read ADC value on a pin selected by HADC_SetReadPin before
    \return ADC value
    \attention HADC_SetReadPin() have to be called success before or unknow result may happen
*/
uint16_t HADC_Read(void){
    HAL_ADC_Start(&_adcHandle);
    HAL_ADC_PollForConversion(&_adcHandle, 100);
    
    return (uint16_t) HAL_ADC_GetValue(&_adcHandle);
}


//============================================================================
/**
	\brief Read ADC value on a pin
	\param[in] pinIndex Index of the pin (look on the board)
	\return 0 if pinIndex>= MAX_SUPPORT_CHANNEL; ADC value otherwise 
*/	
uint16_t HADC_Read(uint8_t pinIndex){
    if (pinIndex >= MAX_SUPPORT_CHANNEL) { return 0; }
    HADC_SetReadPin(pinIndex);
    return HADC_Read();
}
