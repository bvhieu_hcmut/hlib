/**
 \file hlib.cpp
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Implement some global functions for HLib
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#include "hlib.h"


/////////////////////////////////////////////////////////////////////////////////////////////////////
////  Global variables  ////////
hlcd_c      LCD;
huart_c     Serial(1);
huart_c     Serial2(2);
huart_c     Serial3(3);

/////////////////////////////////////////////////////////////////////////////////////////////////////
////  Local function declaration  ////////
static void _SystemClock_Config(void);
static void _Error_Handler(void);


/////////////////////////////////////////////////////////////////////////////////////////////////////
////  Local variables   ////////////////
static volatile  uint32_t   _loopCounter;


/////////////////////////////////////////////////////////////////////////////////////////////////////
////  Public functions implementation //////////////

void HLIB_Reinit(void){
    /* Init ST HAL library */
    HAL_Init();
    
    /* Configure the system clock */
    _SystemClock_Config();
    
    /* enable clock for all GPIO */
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();

    /* remap JTAG to allow using JTDI, JTRST, JTDQ */
    __HAL_RCC_AFIO_CLK_ENABLE();
    __HAL_AFIO_REMAP_SWJ_NOJTAG() ;
    
    /* init HLed */
    HLED_Reinit();
    /* init ADC */
    HADC_Reinit();
}


//============================================================================
/**
 \brief Peforming delay by a finitive loop
 \param numLoop Delay time
 \return None
 \attention Real delay time depends on speed of used MCU.
*/
void HLoopDelay(uint32_t numLoop){
    for (_loopCounter=0; _loopCounter < numLoop; _loopCounter++){
        /* TRICK: use volatile _loopCounter to prevent Compiler Optimization */
    }
}



/////////////////////////////////////////////////////////////////////////////////////////////////////
////  Local functions implementation //////////////
static void _Error_Handler(void){
    while(1) {
    }
}


//============================================================================
static void _SystemClock_Config(void){

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler();
  }

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler();
  }

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}
