/**
 \file hstru.cpp
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Implement string utility functions for HLib
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#include "hstru.h"


//============================================================================
/**
 \brief Convert an unsigned interger to string 
 \param num Converted number
 \param radix Only support 2, 8, 10, 16
 \param outStr Converting result
 \return None
*/
void HSTRU_FromNum(uint32_t num, uint8_t radix, char outStr[]){
  uint32_t quotient, remainder;
  char     storageStr[33];
  int      count, index; //must int type, uint is incorrect

  remainder = num;
  count     = 0;
  do{
    quotient    = remainder % radix;
    remainder   = remainder / radix;
    if (quotient < 10){
      storageStr[count++]  = 0x30 | ((uint8_t) quotient);
    }
    else{
      storageStr[count++]  = (uint8_t) (quotient+55);
    }
  }while (remainder !=0);
  for(index=0; index<count; index++){
    outStr[index] = storageStr[count-index-1];
  }
  outStr[index] = '\0';
}
