/**
 \file hpin.h
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Define functions to control ADC on HLib's base board
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/

#ifndef __HADC_H
#define __HADC_H


#include "htype.h"
#include "hconf.h"


/////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined PLATFORM_BANYAN_BOARD
	#define MAX_SUPPORT_CHANNEL		(9U)
#endif


/////////////////////////////////////////////////////////////////////////////////////////////////////
void        HADC_Reinit(void);
bool_t      HADC_SetReadPin(uint8_t pinIndex);
uint16_t    HADC_Read(void);
uint16_t    HADC_Read(uint8_t pinIndex);

#endif /* __HADC_H */
