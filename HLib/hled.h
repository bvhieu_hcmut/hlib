/**
 \file hled.h
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Define functions to control LEDs on HLib's base boards
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#ifndef __HLED_H
#define __HLED_H

#include "htype.h"
#include "hconf.h"


/////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined PLATFORM_BANYAN_BOARD
    #define NUM_LED             (2U)
    #define LED_GREEN           (0U)
    #define LED_RED             (1U)
#endif


/////////////////////////////////////////////////////////////////////////////////////////////////////
bool_t HLED_Reinit(void);
bool_t HLED_On(uint8_t ledIndex);
bool_t HLED_Off(uint8_t ledIndex);
bool_t HLED_SetState(uint8_t ledIndex, bool_t ledVal);
bool_t HLED_Toggle(uint8_t ledIndex);

#endif /* __HLED_H */
