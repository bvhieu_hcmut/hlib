/**
 \file hlib.h
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Wrap all HLib libraries and define some utility function in HLib
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#ifndef __HLIB_H
#define __HLIB_H

#include "stm32f1xx_hal.h"
#include "hpin.h"
#include "hled.h"
#include "hlcd.h"
#include "hstru.h"
#include "huart.h"
#include "hadc.h"
#include <stdio.h>


/////////////////////////////////////////////////////////////////////////////////////////////////////
extern hlcd_c   LCD;
extern huart_c  Serial;
/////////////////////////////////////////////////////////////////////////////////////////////////////
void HLIB_Reinit(void);
void HLoopDelay(uint32_t numDelay);

#endif /* __HLIB_H */
