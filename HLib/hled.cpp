/**
 \file hled.cpp
 \author Bui Van Hieu <vanhieubk@gmail.com>
 \version 1.0
 \date 2016-20-10
 \brief Implement function to control led on HLib's base boards
 
 \copyright
 This project and all its relevant hardware designs, documents, source codes, compiled libraries
 belong to <b> Smart Sensing and Intelligent Controlling Group (SSAIC Group)</b>. 
 You have to comply with <b> Non-Commercial Share-Alike Creative Common License </b> 
 in order to share (copy, distribute, transmit) or remix (modify, reproduce, adapt) these works.\n
 SSAIC Group shall not be held liable for any direct, indirect or consequential damages 
 with respect to any claims arising from the content of hardware, firmware and/or the use 
 made by customers of the coding information contained herein in connection with their products.\n
*/
#include "hled.h"
#include "hpin.h"


/////////////////////////////////////////////////////////////////////////////////////////////////////
#if defined PLATFORM_BANYAN_BOARD
    #define LED_GREEN_PIN   (34U)
    #define LED_RED_PIN     (35U)
    static uint8_t sArrLedPin[NUM_LED] = {LED_GREEN_PIN, LED_RED_PIN };                                   
#endif



/////////////////////////////////////////////////////////////////////////////////////////////////////
//============================================================================
/**
    \brief Reinit HLed library
    \return TRUE_V
*/
bool_t HLED_Reinit(void){
  uint8_t i;
    for (i=0; i<NUM_LED; i++){
        HPIN_SetMode(sArrLedPin[i], OUTPUT);
        HLED_On(i);
    }

    return TRUE_V;
}


//============================================================================
/**
    \brief Turn on a led
    \param[in] ledIndex Index of the led
    \return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/
bool_t HLED_On(uint8_t ledIndex){
    if (ledIndex >= NUM_LED){ return FALSE_V; }
    HPIN_WriteZero(sArrLedPin[ledIndex]);
    return TRUE_V;
}


//============================================================================
/**
    \brief Turn off a led
    \param[in] ledIndex Index of the led
    \return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/
bool_t HLED_Off(uint8_t ledIndex){
    if (ledIndex >= NUM_LED){ return FALSE_V; }
    HPIN_WriteOne(sArrLedPin[ledIndex]);
    return TRUE_V;
}


//============================================================================
/**
    \brief Set new state for a led
    \param[in] ledIndex Index of the led
    \param[in] ledVal TRUE_V turn on LED. FALSE_V turn off LED
    \return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/
bool_t HLED_SetState(uint8_t ledIndex, bool_t ledVal){ 
    if (ledVal){
        return HLED_On(ledIndex);
    }
    else{
        return HLED_Off(ledIndex);
    }
    
}


//============================================================================
/**
    \brief Toggle state of a led
    \param[in] ledIndex Index of the led
    \return FALSE_V if pinIndex is out of range. TRUE_V otherwise
*/
bool_t HLED_Toggle(uint8_t ledIndex){
    if (ledIndex >= NUM_LED){ return FALSE_V; }
    HPIN_Toggle(sArrLedPin[ledIndex]);
    return TRUE_V;  
}
