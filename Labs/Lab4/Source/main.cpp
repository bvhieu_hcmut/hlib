#include "hlib.h"

#define LCD_D4_PIN      (15)
#define LCD_D5_PIN      (16)
#define LCD_D6_PIN      (17)
#define LCD_D7_PIN      (18)
#define LCD_EN_PIN      (22)
#define LCD_RS_PIN      (23)
#define LCB_BL_PIN      (14)



/////////////////////////////////////////////////////////
////  Global variables  ///////////////////////
uint32_t loopCount;



/////////////////////////////////////////////////////////
////  Local function declaration  ///////////////////////
void Setup(void);
void Loop(void);



/////////////////////////////////////////////////////////
////  Main program  ///////////////////////
int main(){
    HLIB_Reinit();
    Setup();
    while(1){
        Loop();
    }
}


/////////////////////////////////////////////////////////
////  Local function implementation  ////////////////////
void Setup(void){
    LCD.Start( LCD_D4_PIN, LCD_D5_PIN, LCD_D6_PIN, LCD_D7_PIN,
               LCD_EN_PIN, LCD_RS_PIN, LCB_BL_PIN);
    Serial.Start(9600);
    Serial.Println("Wellcome to HLib");
    Serial.Println("This programm send ADC periodic to LCD and UART");
    loopCount = 0;
    HLoopDelay(1000000);
    HPIN_SetMode(4, ANALOG);
    HPIN_SetMode(5, ANALOG);
}


//=======================================================
void Loop(void){
    uint16_t readAdc;
    
    /* read ADC on pin 4 and display on LCD */
    HADC_SetReadPin(4);     //set pin
    readAdc = HADC_Read();  //read ADC
    LCD.Clear();
    LCD.Goto(0, 0);
    LCD.Print("ADC pin 4: ");
    LCD.Print(readAdc);
    
    /* read ADC on pin 5 and send to UART */
    readAdc = HADC_Read(5);
    Serial.Println("ADC pin 5: ", readAdc);
    
    HLoopDelay(10000000);
}

