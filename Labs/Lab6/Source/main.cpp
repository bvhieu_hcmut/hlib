#include "hlib.h"



#define END_OF_FRAME        ('\r')
#define MAX_FRAME_SIZE      (255)
#define PROT_WINDOW_SIZE    (2)

/////////////////////////////////////////////////////////
////  Global variables  ///////////////////////
uint32_t  loopCount;
uint8_t   frameBuff[MAX_FRAME_SIZE];
uint16_t  recvByteCount;

uint8_t   seqSendNext;
uint8_t   seqSendACK;

uint8_t   seqRecvACK;
uint8_t   seqRecvExpect;
/////////////////////////////////////////////////////////
////  Local function declaration  ///////////////////////
void Setup(void);
void Loop(void);
void PROT_HandleFrame(uint8_t buf[], uint16_t bufLen);
void PROT_SendAckFrame(uint8_t seq);
void PROT_SendDataFrame(uint8_t buf[], uint16_t bufLen);

/////////////////////////////////////////////////////////
////  Main program  ///////////////////////
int main(){
    HLIB_Reinit();
    Setup();
    while(1){
        Loop();
    }
}


/////////////////////////////////////////////////////////
////  Local function implementation  ////////////////////
void Setup(void){
    Serial.Start(115200);
    Serial.Println("This programm demo a basic flow control\n");
    loopCount = 0;
    recvByteCount = 0;
    seqSendNext = 0;
    seqSendACK = 0;
    seqRecvExpect = 0;
    seqRecvACK = 0;
}


//=======================================================
void Loop(void){
    uint8_t receiveData;
    
    if (Serial.Available()){
        receiveData = Serial.Read();
        if (receiveData == END_OF_FRAME){   //detect end of frame
            PROT_HandleFrame(frameBuff, recvByteCount);
            recvByteCount = 0;
        }
        else if (receiveData == '\n'){
            Serial.Println("NL");
        }
        else{
            frameBuff[recvByteCount] = receiveData;
            recvByteCount = (recvByteCount + 1) % MAX_FRAME_SIZE;
        }
    }
    
    loopCount++;
    if (loopCount == 500000){
        HLED_Toggle(LED_GREEN);
        loopCount = 0;
    }
}



/////////////////////////////////////////////////////////
////  Local function implementation  ////////////////////
void PROT_HandleFrame(uint8_t buf[], uint16_t bufLen){
    if (bufLen != 1){   //Data frame
        switch (buf[bufLen-1]){
            case '0': 
                seqRecvExpect = 1;
                PROT_SendAckFrame(seqRecvExpect);
                break;
            
            case '1':
                seqRecvExpect = 0;
                PROT_SendAckFrame(seqRecvExpect);
                break;
            
            default: 
                Serial.Println("Wrong format");
                break; //do not process now
        }
    }
}


//=======================================================
void PROT_SendAckFrame(uint8_t seq){
    Serial.Print((char) (seq + 0x30));
    Serial.Print((char) END_OF_FRAME); 
}


//=======================================================
void PROT_SendDataFrame(uint8_t buf[], uint16_t bufLen){
    Serial.Write(buf, bufLen);
    Serial.Print((char) (seqSendNext + 0x30));
    Serial.Print((char) END_OF_FRAME); 
    seqSendNext = (seqSendNext + 1) % PROT_WINDOW_SIZE; /* update sending sequence */
}
