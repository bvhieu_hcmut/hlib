#include "hlib.h"

#define LCD_D4_PIN      (15)
#define LCD_D5_PIN      (16)
#define LCD_D6_PIN      (17)
#define LCD_D7_PIN      (18)
#define LCD_EN_PIN      (22)
#define LCD_RS_PIN      (23)
#define LCB_BL_PIN      (14)



/////////////////////////////////////////////////////////
////  Global variables  ///////////////////////
uint32_t loopCount;



/////////////////////////////////////////////////////////
////  Local function declaration  ///////////////////////
void Setup(void);
void Loop(void);



/////////////////////////////////////////////////////////
////  Main program  ///////////////////////
int main(){
    HLIB_Reinit();
    Setup();
    while(1){
        Loop();
    }
}


/////////////////////////////////////////////////////////
////  Local function implementation  ////////////////////
void Setup(void){
    LCD.Start( LCD_D4_PIN, LCD_D5_PIN, LCD_D6_PIN, LCD_D7_PIN,
               LCD_EN_PIN, LCD_RS_PIN, LCB_BL_PIN);
    Serial.Start(9600);
    Serial.Println("Wellcome to HLib");
    Serial.Println("This programm send back the key you entered");
    loopCount = 0;
}


//=======================================================
void Loop(void){
    uint8_t receiveData;
    if (Serial.Available()){
        receiveData = Serial.Read();
        Serial.Write(receiveData);
    }
    loopCount++;
    if (loopCount == 500000){
        HLED_Toggle(LED_RED);
        loopCount = 0;
    }
}

