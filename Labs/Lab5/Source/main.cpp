#include "hlib.h"



#define END_OF_FRAME        ('\r')
#define MAX_FRAME_SIZE      (255)
/////////////////////////////////////////////////////////
////  Global variables  ///////////////////////
uint32_t  loopCount;
uint8_t   frameBuff[MAX_FRAME_SIZE];
uint16_t  recvByteCount;

/////////////////////////////////////////////////////////
////  Local function declaration  ///////////////////////
void Setup(void);
void Loop(void);


/////////////////////////////////////////////////////////
////  Main program  ///////////////////////
int main(){
    HLIB_Reinit();
    Setup();
    while(1){
        Loop();
    }
}


/////////////////////////////////////////////////////////
////  Local function implementation  ////////////////////
void Setup(void){
    Serial.Start(115200);
    Serial.Println("This programm send back the frame you sent\n");
    loopCount = 0;
    recvByteCount = 0;
}


//=======================================================
void Loop(void){
    uint8_t receiveData;
    
    if (Serial.Available()){
        receiveData = Serial.Read();
        if (receiveData == END_OF_FRAME){   //detect end of frame
            Serial.Write(frameBuff, recvByteCount);
            Serial.Write(END_OF_FRAME);
            recvByteCount = 0;
        }
        else{
            frameBuff[recvByteCount] = receiveData;
            recvByteCount = (recvByteCount + 1) % MAX_FRAME_SIZE;
        }
    }
    
    loopCount++;
    if (loopCount == 500000){
        HLED_Toggle(LED_GREEN);
        loopCount = 0;
    }
}

