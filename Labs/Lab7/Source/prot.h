#ifndef __PROT_H
#define __PROT_H

#include "hlib.h"


//////////////////////////////////////////////////////////////////////////
#define PROT_EOF        ('\r')
#define PROT_ACK        ('A')
#define PROT_NAK        ('N')


//////////////////////////////////////////////////////////////////////////
void PROT_SendData(uint8_t data[], uint16_t dataLength, uint8_t sequence);
void PROT_SendACK(uint8_t sequence);
void PROT_SendNAK(uint8_t sequence);
void PROT_DecodeDataFrame(uint8_t recvBuf[], uint16_t recvLength, char* pData, char* pDataLength, char* pSequence);

#endif /* __PROT_H */
