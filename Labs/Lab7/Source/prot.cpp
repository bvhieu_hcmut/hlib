#include "prot.h"

/**
  * @brief Send data frame
  * @param[in] data Buffer storing data to send
  * @param[in] dataLength Length of data to send
  * @param[in] sequence Sequence of data frame to send
  * @return NONE
*/
void PROT_SendData(uint8_t data[], uint16_t dataLength, uint8_t sequence){
    Serial.Write(data, dataLength);
    Serial.Print(dataLength);
    Serial.Print((char) (sequence + '0'));
    Serial.Print(PROT_EOF);
}


/**
  * @brief Send ACK frame
  * @param[in] sequence Sequence of frame to send
  * @return NONE
*/
void PROT_SendACK(uint8_t sequence){
    Serial.Print(PROT_ACK);
    Serial.Print((char) (sequence + '0'));
    Serial.Print(PROT_EOF);
}


/**
  * @brief Send NAK frame
  * @param[in] sequence Sequence of frame to send
  * @return NONE
*/
void PROT_SendNAK(uint8_t sequence){
    Serial.Print(PROT_NAK);
    Serial.Print((char) (sequence + '0'));
    Serial.Print(PROT_EOF);
}


/**
  * @brief Decode a data frame
  * @param[in] recvBuf Buffer storing received frame
  * @param[in] recvLength Length of received frame in buffer
  * @param[out] pData Pointer to string to hold received data field
  * @param[out] pDataLength Pointer to string to hold data length field
  * @param[out] pSequence Pointer to string to hold sequence field
  * @return NONE
  * @attention User have to provide enough buffer to hold fields
*/
void PROT_DecodeDataFrame(uint8_t recvBuf[], uint16_t recvLength, char* pData, char* pDataLength, char* pSequence){
    uint16_t readPos;
    
    readPos = 0;
    
    /* decode data field */
    for (readPos=0; readPos < recvLength - 3; readPos++){
        pData[readPos] = recvBuf[readPos];
    }
    pData[readPos] = '\0';  //end of string
    
    /* decode data length field */
    pDataLength[0] = recvBuf[readPos];
    readPos++;
    pDataLength[1] = recvBuf[readPos];
    readPos++;
    pDataLength[2] = '\0';

    /* decode sequence field */
    pSequence[0] = recvBuf[readPos];
    pSequence[1] = '\0';
}
