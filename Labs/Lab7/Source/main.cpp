#include "hlib.h"
#include "prot.h"



#define MAX_FRAME_SIZE      (255)


/////////////////////////////////////////////////////////
////  Global variables  ///////////////////////
uint32_t  loopCount;
uint16_t  recvByteCount;
uint8_t   frameBuff[MAX_FRAME_SIZE];
char      dataField[MAX_FRAME_SIZE];
char      dataLengthField[3];
char      sequenceField[2];



/////////////////////////////////////////////////////////
////  Local function declaration  ///////////////////////
void Setup(void);
void Loop(void);


/////////////////////////////////////////////////////////
////  Main program  ///////////////////////
int main(){
    HLIB_Reinit();
    Setup();
    while(1){
        Loop();
    }
}


/////////////////////////////////////////////////////////
////  Local function implementation  ////////////////////
void Setup(void){
    Serial.Start(115200);
    Serial.Println("\nTHIS PROGRAM SEND BACK DECODED FRAME\n");
    Serial.Print("CHECK (ACK 0): "); PROT_SendACK(0);
    Serial.Print("\nCHECK (ACK 1): "); PROT_SendACK(1);
    Serial.Print("\nCHECK (NAK 0): "); PROT_SendNAK(0);
    Serial.Print("\nCHECK (NAK 1): "); PROT_SendNAK(1);
    Serial.Print("\nCHECK (DATA 0): "); PROT_SendData((uint8_t*) "WELLCOME TO HLIB", 16, 0);
    Serial.Print("\nCHECK (DATA 1): "); PROT_SendData((uint8_t*) "YOU CAN DO IT!", 14, 1);
    
    loopCount = 0;
    recvByteCount = 0;

}


//=======================================================
void Loop(void){
    uint8_t receiveData;
    
    if (Serial.Available()){
        receiveData = Serial.Read();
        if (receiveData == PROT_EOF){   //detect end of frame
            if (recvByteCount > 2){ //trivial (not always correct) to detect data frame
                PROT_DecodeDataFrame(frameBuff, recvByteCount, 
                                     &dataField[0], &dataLengthField[0], &sequenceField[0]);
                Serial.Println("DECODED FRAME");
                Serial.Println("Data field: ", dataField);
                Serial.Println("Data length field: ", dataLengthField);
                Serial.Println("Sequence field: ", sequenceField);
            }
            recvByteCount = 0;
        }
        else{
            frameBuff[recvByteCount] = receiveData;
            recvByteCount = (recvByteCount + 1) % MAX_FRAME_SIZE;
        }
    }
    
    loopCount++;
    if (loopCount == 500000){
        HLED_Toggle(LED_GREEN);
        loopCount = 0;
    }
}

